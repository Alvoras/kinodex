$(document).ready(function () {
   var socket = io.connect("http://localhost:8080");

   // PREVENT THE PAGE RELOADING
   $("#search-form").submit(function () {
      return false;
   });

   // SYNC THE SEARCH BAR APPEARANCE WITH IT'S CONTENT AT THE PAGE LOADING
   formInputAnimate();
   fearSearchButton();

   $("body").keypress(function (event) {
      if (event.keyCode === 27) { // ESCAPE KEYCODE
          collapseResults();
      }
   });

   $("#search-button").click(function (event) {
      if ($("#search-field").val() != "") {
         expandResults(event);
         fetchData(socket);
      }
      bubble();
   });

   $("#search-field").click(function (event) {
      event.stopPropagation();
   });

   $(".action-button").click(function (event) {
      event.stopPropagation();
   });

   $("#search-field").change(function () {
      formInputAnimate();
      fearSearchButton();
   });

   $("#search-field").keypress(function (event) {
      if (event.keyCode == 13 && $("#search-field").val() != "") { // IF "ENTER" IS PRESSED AND THE SEARCH FIELD IS NOT NULL
         expandResults(event);

         // FETCH DATA ON ENTER KEYPRESS
         fetchData(socket)
         bubble();
      }

      fearSearchButton();
   });

   $(".overlay").click(function () {
      toggleFilmChoice($(this));
   });

   $(".action-button-container .fa-check-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         return;
      }

      let mode = "is_viewed";
      let film = [];
      film["title"] = $(this).parent().parent().find("p").html();

      if ($(this).parent().parent().find(".badge").hasClass("viewed")) {
         var data = {is_viewed: 1};
         markAsNotViewed($(this));
      }else {
         var data = {is_viewed: 0};
         markAsViewed($(this));
      }
      manageDb(data, mode, film);
   });

   $(".action-button-container .fa-times-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         // DISPLAY BLOCK INSTEAD ?
         return;
      }

      let mode = "is_deleted";
      let film = [];
      film["title"] = $(this).parent().parent().find("p").html();
      let data = {is_deleted: 0};
      manageDb(data, mode, film, $(this));
      removeFromList($(this));
   });

   $("header").click(function (event) {
      if ($("#result-row").children().length > 0) {
         if ($("#result-row").hasClass('expanded')) {
            collapseResults();
         }else {
            expandResults(event);
         }
      }else {
         collapseResults();
      }
   });

   $("#sortby, #arrow-container").click(function (event) {
      event.stopPropagation();
      toggleArrow();
      toggleDropdown();
   })

   $("#sortby-dropdown-item").click(function (event) {
      event.stopPropagation();
   });

   $(".sortby-dropdown-item").click(function () {
      let sortbyValue = $(this).html().trim();

      $("#sortby-type").html(sortbyValue);

      if (sortbyValue === "Viewed") {
         loadNewMainCardList("viewed");
      }else if (sortbyValue === "Not viewed") {
         loadNewMainCardList("not-viewed");
      }else if (sortbyValue === "All") {
         loadNewMainCardList("all");
      }
   });

   $('.sortby-dropdown-item').hover(function () {
      $(this).addClass('active').css("color", "rgba(0, 0, 0, 0.7)");
   }, function () {
      $(this).removeClass('active').css("color", "rgba(255, 255, 255, 0.7)");
   });

});
