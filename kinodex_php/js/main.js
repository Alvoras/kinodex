$(document).ready(function () {
   formInputAnimate();
   fearSearchButton();

   $("body").keypress(function (event) {
      if (event.keyCode === 27) { // ESCAPE KEYCODE
          collapseResults();
      }
   });

   $("#search-button").click(function (event) {
      if ($("#search-field").val() != "") {
         expandResults(event);
         fetchData();
      }
      bubble();
   });

   $("#search-field").click(function (event) {
      event.stopPropagation();
   });

   $(".action-button").click(function (event) {
      event.stopPropagation();
   });

   $("#search-field").change(function () {
      console.log($("#search-field").val());
      formInputAnimate();

      fearSearchButton();
   });

   $("#search-field").keypress(function (event) {
      if (event.keyCode == 13 && $("#search-field").val() != "") { // ENTER KEYCODE
         expandResults(event);
         fetchData();
         bubble();
      }

      fearSearchButton();
   });

   $(".overlay").click(function () {
      toggleFilmChoice($(this));
   });

   $(".action-button-container .fa-check-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         return;
      }
      var title = $(this).parent().parent().find("p").html();

      if ($(this).parent().parent().find(".badge").hasClass("viewed")) {
         var mode = "is_viewed";
         markAsNotViewed($(this));
      }else {
         var mode = "not_viewed";
         markAsViewed($(this));
      }
      manageFilm($(this), mode, title);
   });

   $(".action-button-container .fa-times-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         return;
      }

      var mode = "delete";
      var title = $(this).parent().parent().find("p").html();
      manageFilm($(this), mode, title);
      removeFromList($(this));
   });

   $("header").click(function (event) {
      if ($("#result-row").children().length > 0) {
         if ($("#result-row").hasClass('expanded')) {
            collapseResults();
         }else {
            expandResults(event);
         }
      }else {
         collapseResults();
      }
   });
});
