function expandResults(e) {
    e.stopPropagation();
    $("#result-row").css("top", "0");
    $("#result-row").addClass("expanded");
    $("body").css("padding-top", "300px");
    $("header").css("cursor", 'n-resize');
}
function collapseResults() {
    $("#result-row").css("top", "-300px");
    $("#result-row").removeClass("expanded");
    $("body").css("padding-top", "0");
    $("header").css("cursor", 's-resize');
}
function displayLoader(type) {
    searchPurge();
    $("#result-row").css("justify-content", "center");
    $("#result-row").append('<div id="loader"></div>');
}
function hideLoader() {
    var loader = document.getElementById('loader');
    $("#loader").css("opacity", "0");
    setTimeout(function () {
        loader.parentElement.removeChild(loader.parentElement.children[0]);
        $("#result-row").css("justify-content", "flex-start");
    }, 500);
}
function readyChangeState(el, icon) {
    var addButton = el.children[0].children[1];

    if (icon === "refresh") {
        icon ="refresh fa-spin";
    }

    $(addButton).css("font-size", "0");
    setTimeout(function () {
        addButton.setAttribute("class", "fa fa-"+icon+" add-button");
        $(addButton).css("font-size", "68px");
    }, 75);
}
function formInputAnimate(){
    var searchField = document.getElementById('search-field');
    if (searchField.value != "") {
        $("#search-field-bar").addClass("expand");
        $("#search-field-label").addClass("search-field-filled");
    }else {
        $("#search-field-bar").removeClass("expand");
        $("#search-field-label").removeClass("search-field-filled");
    }
}
function toggleFilmChoice(el) {
    var filmTitle = $(el).find(".film-title");
    var captionContainer = $(el).find(".caption-container");
    var background = $(el).parent().find(".action-overlay");
    var actionButtonContainer= $(el).find(".action-button-container");

    if ( !($(background).hasClass("blur")) ) {
        // FADE IN FILM SPECS
        $(filmTitle).addClass("dispel");
        $(captionContainer).addClass("dispel");

        // GROW AND BLUR BACKGROUND
        $(background).addClass("summon blur");
        $(el).addClass("uncloak");

        // FADE IN BUTTONS
        $(actionButtonContainer).addClass("summon-buttons");
    }else{
        // FADE OUT FILM SPECS
        $(filmTitle).removeClass("dispel");
        $(captionContainer).removeClass("dispel");

        // SHRINK AND CLEAR BACKGROUND
        $(background).removeClass("summon blur");
        $(el).removeClass("uncloak");

        // FADE OUT BUTTONS
        $(actionButtonContainer).removeClass("summon-buttons");
    }
}
// --- TO OPTIMIZE -> BOTH FUNCTIONS BELOW
function markAsViewed( el ) {
    $(el).css("color", "green");
    $(el).parent().parent().find(".badge").removeClass("fa-check-circle-o").addClass("fa-check-circle viewed");
}
function markAsNotViewed( el ) {
    $(el).css("color", "white");
    $(el).parent().parent().find(".badge").removeClass("fa-check-circle viewed").addClass("fa-check-circle-o");
}
function removeFromList( el ) {
    el = $(el).parent().parent().parent();
    $(el).css("transform", "scale(0)");
    setTimeout(function () {
        $(el).css("display", "none");
    }, 300);
}
function fearSearchButton() {
    if ($("#search-field").val().length > 14) {
       $("#search-button").css("right", "-115px");
    }else {
       $("#search-button").css("right", "0");
    }
}
function bubble() {
    if ($("#search-button-bubble").hasClass("bubble")) {
        $("#search-button-bubble").removeClass("bubble");
    }
    setTimeout(function () {
        $("#search-button-bubble").addClass("bubble");
    }, 30);
}

// --- ADD ADAPTATIVE THUMBNAIL SIZE DEPENDING ON THE IMAGE FILE TO KEEP PROPORTIONS --- 
