var express = require('express');
var router = express.Router();
const mysql = require('mysql');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json'));

let dbParams = {
  host: config.server.host,
  user: config.mysql.user,
  password: config.mysql.password,
  database: config.mysql.database
};

// SETUP MYSQL CONNECTION
var connection = mysql.createConnection({
   host: config.server.host,
   user: config.mysql.user,
   password: config.mysql.password,
   database: config.mysql.database
})

connection.connect();


/* GET home page. */
router.get('/', function(req, res, next) {
  connection.query("SELECT * FROM films", (error, results, fields) => {
    if (error) throw error;

    let emptyState = 0;
    if (results.length === 0) emptyState = 1;

    console.log(results);
    res.render('index', { isEmpty: emptyState, data: results });
  });
});

module.exports = router;
