   <head>
      <meta charset="utf-8">
      <title></title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,900,900i" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300i,300,400,500,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <script src="js/jquery-3.2.0.min.js"></script>
      <?php
         require_once "functions.php";
         require_once "conf.inc.php";
       ?>
