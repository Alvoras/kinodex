<?php
    require_once "conf.inc.php";
    require_once "functions.php";

    $mode = "execute";

    if ($_POST["m"] === "add") {
        $req = $_POST["title"];

        include "imdb.php";

        $query = "INSERT INTO films(title, poster_th, poster_lg, director, year) VALUES ('".$film_data['title'][0]."','".$film_data['poster-th'][0]."','".$film_data['poster-lg'][0]."','".$film_data['director'][0]."','".$film_data['year'][0]."')";
        $res = queryDatabase($mode, $query);
    }elseif ($_POST["m"] === "delete") {
        $query = "UPDATE films SET is_deleted=1 WHERE title='".$_POST['title']."'";
        $res = queryDatabase($mode, $query);
    }elseif ($_POST["m"] === "restore") {
        $query = "UPDATE films SET is_deleted=0 WHERE title='".$_POST['title']."'";
        $res = queryDatabase($mode, $query);
    }elseif ($_POST["m"] === "check") {
        $mode = "select";
        $query = "SELECT is_deleted FROM films WHERE title='".$_POST["title"]."'";
        $res = queryDatabase($mode, $query);
    }elseif ($_POST["m"] === "viewed") {
        if ($_POST["v"] === "0") {
            $viewed_state = 1;
        }else {
            $viewed_state = 0;
        }
        $query = "UPDATE films SET viewed=".$viewed_state." WHERE title='".$_POST['title']."'";
        $res = queryDatabase($mode, $query);
    }

    if (!$res){
        echo "-1";
    }else {
        echo $res[0][0];
    }
