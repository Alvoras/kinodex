<?php
function connectDb(){
	try{
		$db = new PDO("mysql:host=".DB_HOST.";charset=utf8;dbname=".DB_NAME, DB_USER, DB_PWD);
	}catch(Exception $e){
		die("ERREUR sql : ".$e->getMessage());
	}
	return $db;
}
function queryDatabase($mode, $request) {
	$db = connectDb();

	if ($mode === "select") {
		$query = $db->prepare($request);

		if ( $result = $query->execute() ) {
			while($row = $query->fetch(PDO::FETCH_NUM)) {
				$rows[] = $row;
			}

			if (isset($rows)) {
				return $rows;
			}
		}else{
			return "0";
		}
	}elseif ($mode === "execute") {
		$query = $db->prepare($request);

		if( !$query->execute() ) {
			return "0";
		}
		return "1";
	}
}
function printArray($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}
