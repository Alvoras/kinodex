<?php
    require_once "functions.php";

    $url = "http://www.theimdbapi.org/api/find/movie?title=";

    $timeout = 20;

    // $REQ IS EITHER DEFINED BY MANAGEDB.PHP IN THE CASE OF A NEW FILM TO REGISTER ("ADD")
    // OR VIA A GET REQUEST
    // IF REQ IS NOT SET THEN IT MEANS THAT WE DO NOT NEED TO RETRIEVE 25 RESULTS BUT ONLY ONE

    if ( !isset($req) ) {
        $req = $_GET["r"];
        $limit = 5;
    }else {
        $limit = 1;
    }

    $req = str_replace(" ", "+", $req);

    $url = $url.$req;

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $html = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($http_code >= 200 && $http_code < 300) {
        $data = json_decode($html);

        //printArray($data);

        for ($i=0; $i < $limit; $i++) {
            // TO STOP THE LOOP IF WE FIND LESS THAN $LIMIT RESULTSS
            if ( !isset($data[$i]->{'title'}) ) {
                break;
            }
            // DO NOT DISPLAY RESULTS WITH INCOMPLETE DATA
            if (empty($data[$i]->{'title'}) ||empty($data[$i]->{'poster'}->{'thumb'}) ||empty($data[$i]->{'poster'}->{'large'}) ||empty($data[$i]->{'director'}) ||empty($data[$i]->{'year'}))
            {
                continue;
            }

            $film_data["title"][] = $data[$i]->{'title'};
            $film_data["poster-th"][] = $data[$i]->{'poster'}->{'thumb'};
            $film_data["poster-lg"][] = $data[$i]->{'poster'}->{'large'};
            $film_data["director"][] = $data[$i]->{'director'};
            $film_data["year"][] = $data[$i]->{'year'};
        }

        if (!empty($_GET)) {
            $film_data = json_encode($film_data);

            echo $film_data;
        }else{
            // DEBUG
            // printArray($film_data);
        }
    }else {
        echo "0";
        echo "IMDb api request failed\n";
        echo "$url\n";
        echo "$http_code";
        exit();
    }
