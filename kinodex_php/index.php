<!DOCTYPE html>
<html>
      <meta charset="utf-8">
      <?php
         require_once "php/conf.inc.php";
         include "php/head.php";
       ?>
       <link rel="stylesheet" href="css/front.css"/>
   </head>
   <body>
      <div id="result-row">

      </div>
       <header>
          <div id="search-container">
             <input type="text" name="search" value="" id="search-field">
             <span class="bar" id="search-field-bar"></span>
             <label id="search-field-label">Rechercher</label>

             <div id="search-button">
                <div id="search-button-bubble"></div>
                <p id="search-button-label"><span id="search-button-label-text">Rechercher</span></p>
             </div>
          </div>
       </header>
         <section id="main-container">
            <?php
            $mode = "select";
            $query = "SELECT * FROM films";
            $data = queryDatabase($mode, $query);
            if (!is_null($data)) {

               foreach ($data as $key => $value) {
                  if (/*$value[5] != 1 && */$value[6] != 1) { // IF VIEWED != 1 AND IS_DELETED != 1
                     // GET LARGE THMB FOR LARGE POSTER IMAGE
                     // MODIFY IMDB.PHP
                     ?>
                     <div class="film-container card">
                        <div class="overlay">
                           <p class="film-title"><?php echo $value[0] ?></p>
                           <div class="caption-container">
                              <p class="film-director"><?php echo $value[3] ?></p>,
                              <p class="film-year"><?php echo $value[4] ?></p>
                           </div>
                           <?php echo ($value[5])?'<i class="fa fa-check-circle badge viewed"></i>':'<i class="fa fa-check-circle-o badge"></i>';

                           echo '<div class="action-button-container">';

                           echo ($value[5])?'<i class="fa fa-check-circle action-button viewed"></i>':'<i class="fa fa-check-circle action-button"></i>';
                           ?>
                              <i class="fa fa-times-circle action-button"></i>
                           </div>
                        </div>
                        <div class="action-overlay" style="background-image: url('<?php echo $value[2] ?>')">
                        </div>
                     </div>
                     <?php
                  }
               }
         }else{
            echo "<p>
               Aucun film sélectionné
            </p>";
         }
            ?>
         </section>
      <script src="js/main.js"></script>
      <script src="js/imdb.js"></script>
      <script src="js/animations.js"></script>
   </body>
</html>
