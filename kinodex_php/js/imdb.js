function toggleFilm(el) {
   readyChangeState(el, "refresh");

   setTimeout(function () {

   }, 400);

   var overlayContent = $(el).children().children();
   console.log(overlayContent);

   var title = overlayContent[0].innerHTML;

   var request = new XMLHttpRequest();
   // CHECK REQUEST TO SEE IF THE FILM IS ALREADY REGISTERED AND IN WHICH STATE
   request.open('POST', "php/manageDb.php");
   request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

   var params = [];

   request.onreadystatechange = function() {
      if (request.readyState == 4) {
         if (request.status == 200) {
            console.log(request.responseText);

            if (request.responseText === "1") {
               // IF IS_DELETED == 1 THEN RESTORE
               console.log("mode = restore");
               var mode = "restore";
            }else if (request.responseText === "0") {
               console.log("mode = delete");
               // IF IS_DELETED == 0 THEN DELETE
               var mode = "delete";
            }else if (request.responseText === "-1") {
               // IF FILM IS NOT PRESENT IN DB THEN CREATE ROW
               console.log("mode = add");
               var mode = "add";
            }

            // IF CHECK REQUEST RETURNS SOMETHING THEN
            // CALL FUNCTION TO ADD FILM TO DB VIA AJAX
            manageFilm(el, mode, title);
         }else{
            console.error(params);
         }
      }
   }

   params.push("m=check");
   params.push("title="+title);
   request.send(params.join('&'));
}
function fetchData() {
   var search = document.getElementById('search-field').value;
   var request = new XMLHttpRequest();
   var baseUrl = 'php/imdb.php?r=';
   var url = baseUrl+search;

   // DISPLAY LOADER
   displayLoader();

   request.open('GET', url);
   request.setRequestHeader('Content-Type', 'application/html');

   request.onreadystatechange = function() {
      if (request.readyState == 4) {
         if (request.status == 200) {
            console.log(request.responseText);
            // HARDCODÉ MOCHE
            // À MODIFIER
            if (request.responseText[0] === "0") {
               // SERVER ERROR
               // DISPLAY ERROR NOTIFICATION
               // WIP
               console.log("SERVER TIMEOUT");
               hideLoader();
            }else if (request.responseText.length != 16919) {
               // DEFINED IN ANIMATIONS.JS
               hideLoader();
               var result = JSON.parse(request.responseText);
               if (result.length < 1 || search == "") {
                  var text = "No results found";
               }else {
                  console.log(result.title.length);
                  setTimeout(function () {
                     for (var i = 0; i < result.title.length; i++) {
                        // GET FILM DETAILS
                        var film = [];
                        film["title"] = result["title"][i];
                        film["poster-th"] = result["poster-th"][i];
                        film["poster-lg"] = result["poster-lg"][i];
                        film["director"] = result["director"][i];
                        film["year"] = result["year"][i];
                        // CREATE DOM ELEMENTS
                        createTopTile(film);
                     }
                  },500);
               }
           }else {
               // DISPLAY NO RESULTS FOUND MESSAGE
               console.log("No results found");
            }
         }else{
            console.error(search);
         }
      }
   }
   request.send();
}
function manageFilm(el, mode, title) {
   var params = [];

   console.log(title);

   params.push("title="+title);

   if (mode === "restore") {
      params.push("m=restore");
   }else if (mode === "delete") {
      params.push("m=delete");
   }else if (mode === "add") {
      params.push("m=add");
   }else if (mode === "is_viewed") {
      params.push("m=viewed");
      params.push("v=1");
   }else if (mode === "not_viewed") {
      params.push("m=viewed");
      params.push("v=0");
   }

   var request = new XMLHttpRequest();
   request.open('POST', "php/manageDb.php");
   request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

   request.onreadystatechange = function() {
      if (request.readyState == 4) {
         if (request.status == 200) {
            console.log("managedb res = \""+request.responseText+"\"");
            if (request.responseText !== "-1") {
               console.log("manageFilm success");
               readyChangeState(el, "check");
               setTimeout(function () {
                  location.reload();
               }, 1500);

               return "1";
            }else {
               console.log("manageFilm failed");
               readyChangeState(el, "times");

               return "0";
            }
         }else{
            console.error(film["title"]);
         }
      }
   }

   request.send(params.join('&'));
}
function createTopTile(film) {
   var tile = document.createElement('div');
   var parent = document.getElementById('result-row');
   parent.appendChild(tile);
   tile.setAttribute("class", "top-film-tile card");
   tile.setAttribute("onclick", "toggleFilm(this)");
   // SET BACKGROUND IMAGE
   $(".top-film-tile").last().css("background-image", "url("+film["poster-th"]+")");

   var overlay = document.createElement('div');
   tile.appendChild(overlay);
   overlay.setAttribute("class", "overlay");

   var tileTitle = document.createElement('h5');
   overlay.appendChild(tileTitle);
   tileTitle.innerHTML= film["title"];
   tileTitle.setAttribute("class", "overlay-title");

   var addIcon = document.createElement('i');
   overlay.appendChild(addIcon);
   addIcon.setAttribute("class", "fa fa-plus-circle add-button");
   addIcon.setAttribute("aria-hidden", "true");

   var captionContainer = document.createElement('div');
   overlay.appendChild(captionContainer);
   captionContainer.setAttribute("class", "caption-container");

   var captionDirector = document.createElement('p');
   captionContainer.appendChild(captionDirector);
   captionDirector.innerHTML= film["director"]+", ";
   captionDirector.setAttribute("class", "overlay-director");

   var captionYear = document.createElement('p');
   captionContainer.appendChild(captionYear);
   captionYear.innerHTML= film["year"];
   captionYear.setAttribute("class", "overlay-year");

   $(".top-film-tile").last().animate({opacity: '1'}, 1000);
}
function searchPurge() {
   var target = document.getElementById('result-row');
   target.innerHTML='';
}
