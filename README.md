# Kinodex 1.2.3 (WIP)

## <b>Description</b>  
   Manage a list of film that you want to watch and mark them as viewed as you catch up !  

## <b>Please note</b> :
   - Only the node.js app will be updated.

   - The app is currently using an sql database to store film infos. A local database support will be implemented soon (you won't need to create a database beforehand anymore).

   - The default database configuration is : root / toor. Change it in kinodex_php/php/<b>conf.inc.php</b> if you're using the php version, or in the kinodex_node/<b>config.json</b> file if you're using the node.js version.

   - Both app use different APIs to get IMDb data, the node one is much more faster.

## <b>- Node.js </b>
   Please use
    ```npm install``` in kinodex/kinodex_node to install all the dependencies needed for the app to work.

![Kinodex](http://i.imgur.com/ermEzdT.png)
