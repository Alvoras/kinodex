function expandResults(e) {
    e.stopPropagation();
    $("#result-row").css("top", "0");
    $("#result-row").addClass("expanded");
    $("body").css("padding-top", "300px");
}
function collapseResults() {
    $("#result-row").css("top", "-300px");
    $("#result-row").removeClass("expanded");
    $("body").css("padding-top", "0");
}
function displayLoader(type) {
    searchPurge();
    $("#result-row").css("justify-content", "center");
    $("#result-row").append('<div id="loader"></div>');
}
function hideLoader() {
    $("#loader").css("opacity", "0");
    var loader = document.getElementById('loader');
    setTimeout(function () {
        loader.parentElement.removeChild(loader.parentElement.children[0]);
        $("#result-row").css("justify-content", "flex-start");
    }, 500);
}
function readyChangeState(el, icon) {
    var addButton = el.children[0].children[1];

    if (icon === "refresh") {
        icon ="refresh fa-spin";
    }

    $(addButton).css("font-size", "0");
    setTimeout(function () {
        addButton.setAttribute("class", "fa fa-"+icon+" add-button");
        $(addButton).css("font-size", "68px");
    }, 75);
}
function formInputAnimate(){
    var searchField = document.getElementById('search-field');
    if (searchField.value != "") {
        $("#search-field-bar").addClass("expand");
        $("#search-field-label").addClass("search-field-filled");
    }else {
        $("#search-field-bar").removeClass("expand");
        $("#search-field-label").removeClass("search-field-filled");
    }
}
function toggleFilmChoice(el) {
   console.log(el);
    var filmTitle = $(el).find(".film-title");
    var captionContainer = $(el).find(".caption-container");
    var background = $(el).parent().find(".action-overlay");
    var actionButtonContainer= $(el).find(".action-button-container");

    if ( !($(background).hasClass("blur")) ) {
        // FADE IN FILM SPECS
        $(filmTitle).addClass("dispel");
        $(captionContainer).addClass("dispel");

        // GROW AND BLUR BACKGROUND
        $(background).addClass("summon blur");
        $(el).addClass("uncloak");

        // FADE IN BUTTONS
        $(actionButtonContainer).addClass("summon-buttons");
    }else{
        // FADE OUT FILM SPECS
        $(filmTitle).removeClass("dispel");
        $(captionContainer).removeClass("dispel");

        // SHRINK AND CLEAR BACKGROUND
        $(background).removeClass("summon blur");
        $(el).removeClass("uncloak");

        // FADE OUT BUTTONS
        $(actionButtonContainer).removeClass("summon-buttons");
    }
}
function createTopTile(film, socket) {
   let tile = document.createElement('div');
   let parent = document.getElementById('result-row');
   parent.appendChild(tile);
   tile.setAttribute("class", "top-film-tile card not-viewed");
   tile.setAttribute("id", film["id"]);
   // TODO
   // REMOVE ONCLICK FROM HTML
   tile.setAttribute("onclick", "toggleFilm(this, \"is_deleted\")");
   // SET BACKGROUND IMAGE
   tile.style.backgroundImage="url("+film["poster"]+")";

   let overlay = document.createElement('div');
   tile.appendChild(overlay);
   overlay.setAttribute("class", "overlay");

   let tileTitle = document.createElement('h5');
   overlay.appendChild(tileTitle);
   tileTitle.innerHTML= film["title"];
   tileTitle.setAttribute("class", "overlay-title");

   let addIcon = document.createElement('i');
   overlay.appendChild(addIcon);
   addIcon.setAttribute("class", "fa fa-plus-circle add-button");
   addIcon.setAttribute("aria-hidden", "true");

   let captionContainer = document.createElement('div');
   overlay.appendChild(captionContainer);
   captionContainer.setAttribute("class", "caption-container");

   let captionYear = document.createElement('p');
   captionContainer.appendChild(captionYear);
   captionYear.innerHTML= film["year"];
   captionYear.setAttribute("class", "overlay-year");
}
function createMainCard(film) {
   let filmCard = "<div class=\"film-container card\" style=\"opacity: 0\"><div class=\"overlay\"><p class=\"film-title\">"+film["title"]+"</p><div class=\"caption-container\"><p class=\"film-year\">"+film["year"]+"</p></div><i class=\"fa fa-check-circle-o badge\"></i><div class=\"action-button-container\"><i class=\"fa fa-check-circle action-button\"></i><i class=\"fa fa-times-circle action-button\"></i></div></div><div class=\"action-overlay\" style=\"background-image: url(\'"+film["poster"]+"\')\"></div></div>"

      $("#main-container").append(filmCard);
      $(".film-container.card").last().animate({opacity: 1}, 500);

      // WE NEED TO REDECLARE $(".class").click(...) JQUERY TRIGGER WHEN A NEW ELEMENT IS ADDED TO THE DOM
      actualizeActionOverlay();
}
function actualizeActionOverlay() {
   $(".overlay").click(function () {
      toggleFilmChoice($(this));
   });

   $(".action-button").click(function (event) {
      event.stopPropagation();
   });

   $(".action-button-container .fa-check-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         return;
      }

      let mode = "is_viewed";
      let film = [];
      film["title"] = $(this).parent().parent().find("p").html();

      if ($(this).parent().parent().find(".badge").hasClass("viewed")) {
         var data = {is_viewed: 1};
         markAsNotViewed($(this));
      }else {
         var data = {is_viewed: 0};
         markAsViewed($(this));
      }
      manageDb(data, mode, film);
   });

   $(".action-button-container .fa-times-circle").click(function () {
      // PREVENT TRIGGERING IT WHILE THE BUTTONS AREN'T SHOWN
      if ( !($(this).parent().parent().parent().find(".action-overlay").hasClass("blur")) ) {
         // DISPLAY BLOCK INSTEAD ?
         return;
      }

      let mode = "is_deleted";
      let film = [];
      film["title"] = $(this).parent().parent().find("p").html();
      let data = {is_deleted: 0};
      manageDb(data, mode, film, $(this));
      removeFromList($(this));
   });
}
// TODO
// --- TO OPTIMIZE -> BOTH FUNCTIONS BELOW
function markAsViewed( el ) {
    $(el).css("color", "green");
    $(el).parent().parent().find(".badge").removeClass("fa-check-circle-o").addClass("fa-check-circle viewed");
    $(el).parent().parent().parent().toggleClass("viewed");
    $(el).parent().parent().parent().toggleClass("not-viewed");
}
function markAsNotViewed( el ) {
    $(el).css("color", "white");
    $(el).parent().parent().find(".badge").removeClass("fa-check-circle viewed").addClass("fa-check-circle-o");
    $(el).parent().parent().parent().toggleClass("not-viewed");
    $(el).parent().parent().parent().toggleClass("viewed");
}
function removeFromList( el ) {
    let card = $(el).parent().parent().parent();

    $(card).addClass("card-remove");
    setTimeout(function () {
        $(card).css("display", "none");
    }, 500);
}
function fearSearchButton() {
    if ($("#search-field").val().length > 14) {
       $("#search-button").css("right", "-115px");
    }else {
       $("#search-button").css("right", "0");
    }
}
function bubble() {
    if ($("#search-button-bubble").hasClass("bubble")) {
        $("#search-button-bubble").removeClass("bubble");
    }
    setTimeout(function () {
        $("#search-button-bubble").addClass("bubble");
    }, 30);
}
function toggleArrow() {
   $("#left-arrow-leg").toggleClass("arrow-toggle");
   $("#right-arrow-leg").toggleClass("arrow-toggle");
}
function toggleDropdown() {
   $("#sortby-dropdown").toggleClass("dropdown-toggle");
}
function loadNewMainCardList(mode) {
   if (mode === "viewed"){
      $(".viewed").css("display", 'block');
      $(".not-viewed").css("display", 'none');
   }else if (mode === "not-viewed") {
      $(".not-viewed").css("display", 'block');
      $(".viewed").css("display", 'none');
   }else if (mode === "all") {
      $(".not-viewed").css("display", 'block');
      $(".viewed").css("display", 'block');
   }
}
function newNotification(text, type) {
   let notificationContainer = $("#notification-container");

   if ( notificationContainer.hasClass("displayed") ) {
      notificationContainer.removeClass("displayed");
      notificationContainer.css("opacity", "0"); // REMOVE PREVIOUS NOTIFICATION
      notificationContainer.css("transform", "translateX(320px)");
      
      setTimeout(function () {
         notificationContainer.css("opacity", "1"); // RESTORE OPACITY
         displayNotification(text, type);
      }, 500);
   }else {
      displayNotification(text, type);
   }

}
function displayNotification(text, type) {
   let notificationContainer = $("#notification-container");

   notificationContainer.addClass("displayed");

   $("#notification-text").html(text);

   if (type === "success") {
      var icon = "/images/icons8-Checked-50.png";
      notificationContainer.css("background", "#066d1d");
      $("#notification-text-container").css("background", "#0b8c28");
   }else if (type === "fail" ||type === "no-results") {
      var icon = (type === "fail")?"/images/icons8-Cancel-50.png":"/images/icons8-Dislike-50.png";
      notificationContainer.css("background", "#7a0906");
      $("#notification-text-container").css("background", "#a3100b");
   }

   $("#notification-icon").attr("src", icon);

   notificationContainer.css("transform", "translateX(-320px)");
   setTimeout(function () {
      notificationContainer.css("opacity", "0");
      setTimeout(function () {
         notificationContainer.css("transform", "translateX(320px)");
         notificationContainer.removeClass("displayed");
         setTimeout(function () {
            notificationContainer.css("opacity", "1");
         }, 500);
      }, 600);
   }, 3000);

}
