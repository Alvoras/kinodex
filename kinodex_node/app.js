const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
//const imdb = require('imdb-node-api'); OLD API
const imdb = require('imdb-api');
const mysql = require('mysql');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json'));

let dbParams = {
  host: config.server.host,
  user: config.mysql.user,
  password: config.mysql.password,
  database: config.mysql.database
};

const index = require('./routes/index');

const app = express();

// SETUP SOCKET SERVER
const server = require('http').Server(app);
const io = require('socket.io')(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);

// --- CREATE SOCKET ---
io.on('connection', (socket) => {
  socket.on('search', (film) => {
    console.log(film);
    // IMDB API
    imdb.search({title: film}, {apiKey: 'b1d3536d', timeout: 10000}).then( (data) => {
      console.log("here", data);

      socket.emit("result", data);
    } ).catch((err) => {
      /*
      let errorLog = [];
      errorLog["message"] = err.message;
      errorLog["query"] = film;
      console.log(errorLog);
      */
      socket.emit("notFound", err);
    }); // ERROR HANDLER END
  }); // SOCKET ON SEARCH END

  socket.on("probeDB", (data) => {
    var connection = mysql.createConnection(dbParams);
    console.log(data);
    // MYSQL OPEN CONNECTION
    connection.connect();

    // MYSQL QUERY
    connection.query("SELECT "+data.field+" FROM films WHERE title='"+data.title+"'", (error, results, fields) => {
      if (error) throw error;

      // EMIT PROBE RESULT
      if (data.field === "is_deleted") {
        (results.length === 0 && data.field === "is_deleted")? socket.emit("probeDBResult", {is_deleted: -1}):socket.emit("probeDBResult", {is_deleted: results[0].is_deleted});
      }else if (data.field === "is_viewed") {
        socket.emit("probeDBResult", {is_viewed: results[0].is_viewed});
      }
    });

    connection.end();
  });

  // MANAGEDB HANDLING
  socket.on("add", (film) => {
    // film["title"] = film["title"].replace("'", "");
    let connection = mysql.createConnection(dbParams);
    console.log("FILM :",film.title);

    let addQuery = "INSERT INTO films(title, year, poster) VALUES('"+film.title+"', "+film.year+", '"+film.poster+"')";
    console.log(addQuery);
    connection.query(addQuery, (error, results, fields) => {
      let success = 1;
      if (error) throw error, success = 0;
      socket.emit("manageOK", {success: success});
      console.log(results);
    });
    connection.end();
  }); // END OF SOCKET ON ADD

  socket.on("restore", (title) => {
    manageFilmState(0, 0, title); // MARK AS NOT DELETED
  });

  socket.on("delete", (title) => {
    manageFilmState(1, 0, title); // MARK AS DELETED
    manageFilmState(0, 1, title); // MARK AS NOT VIEWED
  });

  socket.on("not_viewed", (title) => {
    manageFilmState(0, 1, title); // MARK AS NOT VIEWED
  });

  socket.on("viewed", (title) => {
    manageFilmState(1, 1, title); // MARK AS VIEWED
  });


  function manageFilmState(value, mode, title) {
    let connection = mysql.createConnection(dbParams);

    let queryField = "is_viewed";
    if ( !(mode) ) {
      queryField = "is_deleted";
    }

    let dbQuery = "UPDATE films SET "+queryField+"="+value+" WHERE title='"+title+"'";
    console.log(dbQuery);

    connection.query(dbQuery, (error, results, fields) => {
      let success = 1;
      if (error) throw error, success = 0;
      socket.emit("manageOK", {success: success});
      console.log(results);
    });
    connection.end();
  } // END OF FUNCTION manageFilmState

}); // END OF SOCKET ON CONNECTION

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

server.listen(config.server.port)

module.exports = app;
